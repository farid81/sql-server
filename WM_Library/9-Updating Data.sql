/* Manipulate Data for WM_Library 
	script Date: March 6, 2019
	Developed by: Farid Eslambolchi, Dmitryi Kryukov, Varut Wongsuwan, Tyler Beland
*/

use WM_Library
;
go


/* update Membership.tblAdults:
2008 -> 2018 by adding 10 years
2009 -> 2019 by adding 10 years
*/

-- using DATEADD (datepart, number,date)
update Membership.tblAdults
set StartDate = DATEADD(year, 10, StartDate)
;
go

update Membership.tblAdults
set ExpiryDate = DATEADD(year, 10, ExpiryDate)
;
go


/* update Services.tblLoan:
2007 -> 2018 by adding 11 years
2008 -> 2019 by adding 11 years
*/

-- using DATEADD (datepart, number,date)
update Services.tblLoan
set OutDate = DATEADD(year, 10, OutDate)
;
go

update Services.tblLoan
set DueDate = DATEADD(year, 10, DueDate)
;
go
