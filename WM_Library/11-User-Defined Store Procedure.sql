/* Using User-Defined Store Procedure fin WM_Library 
	script Date: March 8, 2019
	Developed by: Farid Eslambolchi, Dmitryi Kryukov, Varut Wongsuwan, Tyler Beland
*/

use WM_Library
;
go

/* 1. create a procedure,  Services.NumberOfLoansSP */

create procedure Services.NumberOfLoansSP
(
	@Year as int
)
as	
	
	begin
		Select count(CopyID) as 'Number of Loans'
			from Services.tblLoanHistory
		where year(OutDate) = @Year
	end
;
go


/* call procedure NumberOfLoansSP  */
execute  Services.NumberOfLoansSP 2007
;
go




/* 2. create a procedure, Services.AverageLengthSP, that return the average length of a loan */
create procedure Services.AverageLengthSP

as
  begin
    select AVG(abs(datediff(day, OutDate, InDate)))
		from Services.tblLoanHistory
	end
;
go

drop procedure Services.AverageLengthSP

/* call procedure Services.AverageLengthSP */
execute  Services.AverageLengthSP
;
go



/* 3. create a procedure, Services.PercentageOfOverdueSP, that return percentage of all loans eventually becomes overdue */

create procedure Services.PercentageOfOverdueSP
as
  begin
    --declare a variable
    declare @CountAllLoans as decimal(5,2),
			@CountOverdue as decimal(5,2),
			@Percentage as decimal(5,2)
    select @CountOverdue = count(CONCAT(ISBN, CopyId))
		from Services.tblLoanHistory as SLH
	where SLH.InDate > SLH.DueDate

    select @CountAllLoans = count(CONCAT(ISBN, CopyId))
		FROM Services.tblLoanHistory

	set @Percentage = cast(@CountOverdue as decimal(5,2))*100/@CountAllLoans 
	    --return the result to the function
    select @Percentage as 'Percentage'
  end
;
go

/* call procedure Services.PercentageOfOverdueSP */
execute  Services.PercentageOfOverdueSP
;
go