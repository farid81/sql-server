/* Create Tables WM_Library 
	script Date: February 27, 2019
	Developed by: Farid Eslambolchi, Dmitryi Kryukov, Varut Wongsuwan, Tyler Beland
*/

use WM_Library
;
go


/***** Create Table No. 1 - Membership.tblMembers *****/

create table Membership.tblMembers
(
	MemberID int identity(1,1) not null,
	LastName nvarchar(20) not null,
	FirstName nvarchar(15)not null,
	MiddleInitial nvarchar(1) null,
	Photograph varbinary(max) null,
	constraint pk_tblMembers primary key clustered (MemberID asc)
)
;
go

/***** Create Table No. 2 - Membership.tblAdults *****/
create table Membership.tblAdults
(
	MemberID int not null,
	Address nvarchar(60) not null,
	City nvarchar(15) not null,
	State nchar(2) not null,
	ZIP nvarchar(15) not null,
	PhoneNumber nvarchar (24) null,
	StartDate datetime not null,
	ExpiryDate datetime not null,
	constraint pk_tblAdults primary key clustered (MemberID asc)
)
;
go

/***** Create Table No. 3 - Membership.tblJuveniles *****/
create table Membership.tblJuveniles
(
	MemberID int not null,
	AdultMemberID int not null,  -- Foreign key from tblAdults
	BirthDate datetime not null,
	constraint pk_tblJuveniles primary key clustered (MemberID asc)
)
;
go


/***** Create Table No.4 - Collection.tblAuthor *****/
create table Collection.tblAuthor
(
	AuthorID int identity(1,1) not null,
	FirstName nvarchar(15) not null,
	LastName nvarchar(20) not null,
	Description nvarchar(100) null,
	constraint pk_tblAuthor primary key clustered (AuthorID asc)
)
;
go

/***** Create Table No.5 - Collection.tblTitle *****/
create table Collection.tblTitle
(
	TitleID int identity(1,1) not null,
	Title nvarchar(50) not null,
	AuthorID int not null,  -- Foreign Key from tblAuthor table
	Synopsis nvarchar(500) null,
	constraint pk_tblTitle primary key clustered (TitleID asc)
)
;
go

/***** Create Table No.6 - Collection.tblCopy *****/
create table Collection.tblCopy
(
	CopyID int identity(1,1) not null, 
	ISBN nvarchar(16) not null, -- Foreign Key from tblItem table
	OnLoan bit not null, --use default value = 0
	constraint pk_tblCopy_tblItem primary key clustered
		 (
		  CopyID asc,
		  ISBN asc
		  ) 
)
;
go


/***** Create Table No.7 - Collection.tblItem *****/
create table Collection.tblItem
(
	ISBN nvarchar(16) not null, 
	TitleID int not null,  -- Foreign Key from tblTitle table
	Translation nvarchar(20) not null,
	Cover nvarchar(12) not null,
	Loanable bit not null, -- use default value = 1
	constraint pk_tblItem primary key clustered (ISBN asc)
)	
;
go


/***** Create Table No.8 - Services.tblReservations *****/
create table Services.tblReservations
(
	ReservationID int identity(1,1) not null,
	ISBN nvarchar(16) not null, -- foreign key from tabItem
	MemberID int not null,      -- foreign key from tblMembers
	LogDate datetime not null,
	Note nvarchar(100) null,
	constraint pk_tblReservations primary key clustered	(ReservationID asc)
)
;
go

/***** Create Table No.9 - Services.tblLoan *****/

create table Services.tblLoan
(
	CopyID int not null,        --foreign key from tblCopy
	ISBN nvarchar(16) not null, --foreign key from tblCopy
	MemberID int not null, -- foreign key from tblMembers
	OutDate datetime not null,
	DueDate datetime null,
	constraint pk_tblLoan primary key clustered
		( -- composite primary key
		  CopyID asc,
		  ISBN asc
		  )
)
;
go


/***** Create Table No.10 - Services.tblLoanHistory *****/
create table Services.tblLoanHistory
(
	CopyID int not null,        --foreign key from tblCopy
	ISBN nvarchar(16) not null, --foreign key from tblCopy
	OutDate datetime not null,
	MemberID int not null, -- foreign key from tblMembers
	DueDate datetime not null,
	InDate datetime not null,
	Note nvarchar(100) null,
	constraint pk_tblLoanHistory primary key clustered
		( -- composite primary key
			CopyID asc,
			ISBN asc 
		)
)
;
go

