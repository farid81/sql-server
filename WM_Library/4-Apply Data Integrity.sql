/* Apply Data Integrity for WM_Library 
	script Date: February 27, 2019
	Developed by: Farid Eslambolchi, Dmitryi Kryukov, Varut Wongsuwan, Tyler Beland
*/

use WM_Library
;
go


/***** Table No.9 - Services.tblLoan *****/


/* 1) Between  Services.tblLoan and Collection.tblCopy */
alter table Services.tblLoan
	add constraint fk_tblLoan_tblCopy_CopyID_ISBN foreign key (CopyID, ISBN)
		references Collection.tblCopy (CopyID, ISBN)
;
go

/* 2) Between  Services.tblLoan and Membership.tblMembers */
alter table Services.tblLoan
	add constraint fk_tblLoan_tblMembers foreign key (MemberID)
		references Membership.tblMembers (MemberID)
;
go

/* 3) Adding Check Constraint*/

-- check that due_date >= out_date (as per task 2, number 3)
alter table Services.tblLoan
	add constraint ck_DueDate_tblLoan
		check (DueDate >= OutDate)
;
go

-- default value for OutDate as current date
alter table Services.tblLoan
	add constraint df_OutDate_tblLoan default (getdate()) for OutDate
;
go





/***** Table No. 1 - Membership.tblMembers *****/
-- No extra constraints (only one primary key)


/***** Table No. 2 - Membership.tblAdults *****/

-- Foreign key constraints
/* 1) Between  Membership.tblAdults and Membership.tblMembers */
alter table Membership.tblAdults
	add constraint fk_tblAdults_tblMembers foreign key (MemberID)
		references Membership.tblMembers (MemberID)
;
go	

/*Adding Default Constraint*/

/* 1) Adding default value for state column tblAdults as 'WA' */
alter table Membership.tblAdults
	add constraint df_State_tblAdults default ('WA') for State
;
go

/* 2) Adding default value for phoneNumber column as 'null' */
alter table Membership.tblAdults
	add constraint df_PhoneNumber_tblAdults default ('NULL') for PhoneNumber
;
go

/* 3) Adding Check Constraint*/
/* Adding check constraint between StartDate and ExpiryDate of Membership.tblAdults */
alter table Membership.tblAdults
	add constraint ck_ExpiryDate_StartDate
		check (ExpiryDate >= StartDate)
;
go

	

/***** Table No. 3 - Membership.tblJuveniles *****/
/* 1) Between  Membership.tblJuveniles and Membership.tblMembers */
alter table Membership.tblJuveniles
	add constraint fk_tblJuveniles_tblMembers foreign key (MemberID)
		references Membership.tblMembers (MemberID)
;
go


/* 1) Between  Membership.tblJuveniles and Membership.tblAdults */	
alter table Membership.tblJuveniles
	add constraint fk_tblJuveniles_tblAdults foreign key (AdultMemberID)
		references Membership.tblAdults (MemberID)
;
go		


/***** Table No.4 - Collection.tblAuthor *****/
-- No extra constraints (only one primary key)


/***** Table No.5 - Collection.tblTitle *****/

-- Foreign key constraints
/* 1) Between  Collection.tblTitle and Collection.tblAuthor */
alter table Collection.tblTitle
	add constraint fk_tblTitle_tblAuthor foreign key (AuthorID)
		references Collection.tblAuthor (AuthorID)
;
go


/***** Table No.6 - Collection.tblCopy *****/

-- Foreign key constraints
/* 1) Between  Collection.tblCopy and Collection.tblItem */
alter table Collection.tblCopy
	add constraint fk_tblCopy_tblItem foreign key (ISBN)
		references Collection.tblItem (ISBN)
;
go

-- Default constraints (set OnLoan column value to zero)

alter table Collection.tblCopy
	add constraint df_OnLoan_tblCopy
	default ('N') for OnLoan
;
go



/***** Table No.7 - Collection.tblItem *****/

-- Foreign key constraints
/* 1) Between  Collection.tblItem and Collection.tblTitle */
alter table Collection.tblItem
	add constraint fk_tblItem_tblTitle foreign key (TitleID)
		references Collection.tblTitle (TitleID)
;
go




-- Default constraints (set Loanable column value to zero)


alter table Collection.tblItem
	add constraint df_Loanable_tblItem
	default ('Y') for Loanable
;
go

/***** Table No.8 - Services.tblReservations *****/

-- Foreign key constraints
/* 1) Between  Services.tblReservations and Collection.tblItem */
alter table Services.tblReservations
	add constraint fk_tblReservations_tblItem foreign key (ISBN)
		references Collection.tblItem (ISBN)
;
go

/* 2) Between  Services.tblReservations and Membership_tblMembers */

alter table Services.tblReservations
	add constraint fk_tblReservations_tblMembers foreign key (MemberID)
		references  Membership.tblMembers (MemberID);
go

-- default value for LogDate as current date
alter table Services.tblReservations
	add constraint df_LogDate_tblReservations default (getdate()) for LogDate
;
go


/***** Table No.10 - Services.tblLoanHistory *****/

/* 1) Between  Services.tblLoanHistory and Collection.tblCopy */

alter table Services.tblLoanHistory
	add constraint fk_tblLoanHistory_tblCopy_CopyID_ISBN foreign key (CopyID, ISBN)
		references Collection.tblCopy (CopyID, ISBN)
;
go



/* 2) Between  Services.tblLoanHistory and Membership.tblMembers  */
alter table Services.tblLoanHistory
	with nocheck
	add constraint fk_tblLoanHistory_tblMembers foreign key (MemberID)
		references Membership.tblMembers (MemberID)
;
go



/*SOLUTION*/
/* Modify Primary key for  Services.tblLoanHistory table*/

ALTER TABLE  Services.tblLoanHistory
	add constraint pk_tblLoanHistory primary key clustered
		( -- composite primary key
			CopyID asc,
			ISBN asc ,
			OutDate asc
		);
		go
alter table Services.tblLoanHistory
	drop constraint pk_tblLoanHistory;



