/* Manipulate Data for WM_Library 
	script Date: March 6, 2019
	Developed by: Farid Eslambolchi, Dmitryi Kryukov, Varut Wongsuwan, Tyler Beland
*/

use WM_Library
;
go


/* 1. Create a mailing list of Library members that includes the members� full names and complete address information.*/
 
SELECT concat_ws(', ', MM.LastName, MM.MiddleInitial, MM.FirstName) as "Full Name",
concat_ws(', ', MA.Address, MA.City, MA.State, MA.ZIP) as "Full Address"
FROM Membership.tblJuveniles as MJ
INNER JOIN Membership.tblMembers as MM
ON MJ.MemberID = MM.MemberID
LEFT OUTER JOIN Membership.tblAdults MA
ON  MJ.AdultMemberID = MA.MemberID
	union
SELECT concat_ws(', ', MM.LastName, MM.MiddleInitial, MM.FirstName) as "Full Name",
concat_ws(', ', MA.Address, MA.City, MA.State, MA.ZIP) as "Full Address"
FROM Membership.tblMembers as MM
INNER JOIN Membership.tblAdults MA
ON  MM.MemberID = MA.MemberID
;
go


/* 2. Write and execute a query on the title, item, and copy tables that returns the isbn, copy_no, on_loan, title, translation, and cover, and values for rows in the copy table with an ISBN of 1 (one), 500 (five hundred), or 1000 (thousand).  Order the result by isbn column. */

select CC.ISBN as 'ISBN',
		CC.CopyID as 'Copy ID',
		CC.OnLoan as 'On Loan',
		CT.Title as 'Title of Item',
		CI.Translation as 'Translation',
		CI.Cover as 'Type of Cover'
	from Collection.tblCopy as CC
		inner join Collection.tblItem as CI
			on CC.ISBN = CI.ISBN
		left outer join Collection.tblTitle as CT
			on CI.TitleID = CT.TitleID
	where CC.ISBN in(1, 500, 1000)
;
go

/* 3. Write and execute a query to retrieve the member�s full name and member_no from the member table and the isbn and log_date values from the reservation table for members 250, 341, 1675. Order the results by member_no. You should show information for these members, even if they have no books or reserve.*/

select 	MM.MemberID, 
		concat_ws(', ', MM.LastName, MM.FirstName) as 'Full Name', 
		SR.ISBN as 'ISBN Number', 
		SR.LogDate as 'Log Date'
	from Membership.tblMembers as MM
		left outer join Services.tblReservations as SR
			on MM.MemberID = SR.MemberID
	where	MM.MemberID in (250, 341, 1675)
	order by MM.MemberID
;   
go



select sum(cnt) as 'Total Loans'
from
(
SELECT COUNT(*) as cnt
FROM Services.tblLoan as SL
UNION
SELECT COUNT(*) as cnt
FROM Services.tblLoanHistory as SLH
) as sum_table