/* Apply Data Integrity for WM_Library 
	script Date: March 5, 2019
	Developed by: Farid Eslambolchi, Dmitryi Kryukov, Varut Wongsuwan, Tyler Beland
*/

use WM_Library
;
go

/* 1. create non clustered index (ncl_Title_tblTitle) on the Title table for Title */
create nonclustered index ncl_Title_tblTitle
		on Collection.tblTitle (Title)
;
go


/* 2. create non clustered index (ncl_LastName_tblAuthor) on the Author table for LastName */
create nonclustered index ncl_LastName_tblAuthor
		on  Collection.tblAuthor (LastName)
;
go

