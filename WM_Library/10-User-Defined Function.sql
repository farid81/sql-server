/* Creating Views for WM_Library 
	script Date: March 7, 2019
	Developed by: Farid Eslambolchi, Dmitryi Kryukov, Varut Wongsuwan, Tyler Beland
*/

use WM_Library
;
go

/* 1. How many loans did the library do last year? */

create function Services.NumberOfLoansFn
(
	@Year as int
)
returns int
as	
	
	begin
		declare @NumberOfLoans as int
		select @NumberOfLoans = count(CopyID)
			from Services.tblLoanHistory
		where year(OutDate) = @Year
		return @NumberOfLoans
	end
;
go

select Services.NumberOfLoansFn(2007) as 'Number Of Loans'
;
go


/* 2. What percentage of the membership borrowed at least one book? */

create function Membership.PercentageHaveLoaned
(
)
returns float
as
  begin
    --declare a variable
    declare @CountAll as float,
			@CountLoaned as float,
			@Percentage as float
    select @CountLoaned = COUNT(MM.MemberID)
		FROM Membership.tblMembers as MM
		INNER JOIN Services.tblLoan as SL
		ON MM.MemberID = SL.MemberID

    select @CountAll = COUNT(MemberID)
		FROM Membership.tblMembers

	select @Percentage = cast((@CountLoaned*100.00/@CountAll) as float)
		
	    --return the result to the function
  
    return @Percentage
  end
;
go

drop function Membership.PercentageHaveLoaned
 
-- testing Membership.PercentageHaveLoaned
select Membership.PercentageHaveLoaned() as 'Percentage'
;
go



/* 3.	What was the greatest number of books borrowed by any one individual? */
create view Services.CopyCountHighest
	as
		SELECT TOP (1000) COUNT(SL.CopyID) as 'Copy Count',
					CONCAT_WS(' ', MM.FirstName, MM.MiddleInitial, MM.LastName) as 'Full Name'
			FROM Services.tblLoan as SL
				INNER JOIN Membership.tblMembers as MM
					ON SL.MemberID = MM.MemberID
			GROUP BY SL.MemberID, CONCAT_WS(' ', MM.FirstName, MM.MiddleInitial, MM.LastName)
		UNION
		SELECT TOP(1000) COUNT(SLH.CopyID) as 'Copy Count',
		CONCAT_WS(' ', MM.FirstName, MM.MiddleInitial, MM.LastName) as 'Full Name'
			FROM Services.tblLoanHistory as SLH
				INNER JOIN Membership.tblMembers as MM
					ON SLH.MemberID = MM.MemberID
		GROUP BY SLH.MemberID, CONCAT_WS(' ', MM.FirstName, MM.MiddleInitial, MM.LastName)
		ORDER BY COUNT(SLH.CopyID) desc, SLH.MemberID asc
;
go


SELECT TOP(1) [Full Name], MAX([Copy Count]) as '# of Loaned Items'
	FROM Services.CopyCountHighest
	GROUP BY [Full Name]
	ORDER BY '# of Loaned Items' DESC
;
go

/* 4. What percentage of the books was loaned out at least once last year? */


/* create view for Services.loanedBooksView */

create view Services.loanedBooksView
WITH ENCRYPTION
as
select Copyid, ISBN, count(OutDate) as 'OutDate'
	from Services.tblLoanHistory
	where year(OutDate) = 2007
	group by CopyID, ISBN
;
go




create function Services.getPercentageLoanedOnceFn
(
)
returns float
as
  begin
    --declare a variable
    declare @TotalLoanedCopy as float,
			@TotalCopy as float,
			@Percentage as float
	select @TotalLoanedCopy = count(CopyID)
			from [Services].[loanedBooksView]
	select @TotalCopy = count(CopyID)
			from Collection.tblCopy
	 --@Percentage = cast(@TotalLoanedCopy as decimal(2,6))*100/@TotalCopy
	select  @Percentage = cast((@TotalLoanedCopy*100.00/@TotalCopy) as float)

	--select @Percentage = convert(decimal(10,5),(100 * convert(float,@TotalLoanedCopy)/convert(float, @TotalCopy)))
	    --return the result to the function
  
    return @Percentage
  end
;
go


drop function Services.getPercentageLoanedOnceFn


-- testing Services.getPercentageLoanedOnceFn */
select Services.getPercentageLoanedOnceFn() as 'Percantage'
;
go




/* 5. What percentage of all loans eventually becomes overdue? */

create function Services.PercentageOfOverdueFN
(
)
returns decimal(5,2)
as
  begin
    --declare a variable
    declare @CountAllLoans as decimal(5,2),
			@CountOverdue as decimal(5,2),
			@Percentage as decimal(5,2)
    select @CountOverdue = count(CONCAT(ISBN, CopyId))
		from Services.tblLoanHistory as SLH
	where SLH.InDate > SLH.DueDate

    select @CountAllLoans = count(CONCAT(ISBN, CopyId))
		FROM Services.tblLoanHistory

	select @Percentage = cast(@CountOverdue as decimal(5,2))*100/@CountAllLoans 
	    --return the result to the function
  
    return @Percentage
  end
;
go

drop function Services.PercentageOfOverdueFN

select Services.PercentageOfOverdueFN() as 'Loans Overdue percentage'
;
go



/* 6. What is the average length of a loan? */

create function Services.AverageLengthFn
(
)
returns int
as
  begin
    
    declare @NumberOfDay as int,
			@Average as int
    select @NumberOfDay = abs(datediff(day, OutDate, InDate))
		from Services.tblLoanHistory
	select @Average = AVG(@NumberOfDay)
	    --return the result to the function
  
    return @Average
  end
;
go

-- testing Services.AverageLengthFn
select Services.AverageLengthFn() as 'Average'



/* 7. What are the library peak hours for loans? */

create function Services.LoanPeakHoursFN
(
	@Year as int
)
returns table
as
  return
		(
		select RIGHT('0'+CAST(DATEPART(hour, OutDate) as varchar(2)),2) + ':' +
				RIGHT('0'+CAST(DATEPART(minute, OutDate)as varchar(2)),2) as 'Hour',
				count(SL.CopyID) as 'Peak'
		from Services.tblLoan as SL
		where year(OutDate) = @Year
		group by RIGHT('0'+CAST(DATEPART(hour, OutDate) as varchar(2)),2) + ':' +
				 RIGHT('0'+CAST(DATEPART(minute, OutDate)as varchar(2)),2)
		)
;
go

 select *
 from Services.LoanPeakHoursFN(2007)

 /* peak Dates for loans */

create function Services.getPeakDaysFh
	( -- declare parametr
		@Year as int
	)
returns table
as	
	return
		(
		select  SL.OutDate as 'Date',
			count(SL.CopyID) as 'Total'
			from Services.tblLoan as SL
			group by SL.OutDate
			having year(SL.OutDate) = @Year
		)
;
go

-- testsing the Services.PeakHoursFh
select *
	from Services.getPeakDaysFh(2007)
;
go