/* Create Database WM_Library 
	script Date: February 27, 2019
	Developed by: Farid Eslambolchi, Dmitryi Kryukov, Varut Wongsuwan, Tyler Beland
*/

--switch to master database
use master
;
go


drop database WM_Library
;
go

--create database WM_Library
create database WM_Library
on primary
(
	name = 'WM_Library',
	filename = 'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\WM_Library',
	size = 15MB,
	filegrowth = 3MB,
	maxsize = 100MB
)
log on
(
	name = 'Library_log',
	filename = 'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\WM_Library_log',
	size = 3MB,
	filegrowth = 10%,
	maxsize = 25MB
)
;
go