/* Apply Data Integrity for WM_Library 
	script Date: March 5, 2019
	Developed by: Farid Eslambolchi, Dmitryi Kryukov, Varut Wongsuwan, Tyler Beland
*/

use WM_Library
;
go


/* ALTER tblMembers */
ALTER TABLE Membership.tblMembers
	alter column Photograph nvarchar(100) null;

/* 1. Import Membership.tblMembers */
BULK INSERT Membership.tblMembers
FROM 'C:\Users\ipd\Desktop\DataBase2\WM_Library\Uploads\member_data.csv'
with(
	format = 'CSV',
	firstrow = 1,
	rowterminator = '\n'	
)
;

go
select *
	from Membership.tblMembers;
	go


/* 2. Import Membership.tblAdults */
BULK INSERT Membership.tblAdults 
FROM 'C:\Users\ipd\Desktop\DataBase2\WM_Library\Uploads\adult_data.csv'
with(
	format = 'CSV',
	firstrow = 1,
	rowterminator = '\n'
)
;
go

select *
	from Membership.tblAdults;
	go


/* 3. Import Membership.tblJuveniles */
BULK INSERT Membership.tblJuveniles
FROM 'C:\Users\ipd\Desktop\DataBase2\WM_Library\Uploads\juvenile_data.csv'
with(
	format = 'CSV',
	firstrow = 1,
	rowterminator = '\n'
)
;
go

select *
	from Membership.tblJuveniles
;
go

/* 4. Import Collection.tblAuthor */

set identity_insert Collection.tblAuthor on
;
go

insert into Collection.tblAuthor (AuthorID, FirstName, LastName, Description)
	values (1, 'James', 'Herbert',null),
			(2, 'JD', 'Salinger', null),
			(3, 'JK', 'Rowling', null),
			(4, 'Rudyard', 'Kipling', null),
			(5, 'Jonathan', 'Swift', null),
			(6, 'Ronald', 'Barthes', null),
			(7, 'Jodie', 'Picoult', null),
			(8, 'Jonathan', 'Lethem', null),
			(9, 'Thomas', 'Pinchon', null),
			(10, 'Adrian', 'Tomine', null)
;
go

set identity_insert Collection.tblAuthor off
;
go

select *
	from Collection.tblAuthor
;
go



/* 5. Import Collection.tblTitle */

ALTER TABLE Collection.tblCopy
	alter column Title nvarchar(60) not null
;
go

BULK INSERT Collection.tblTitle
FROM 'C:\Users\ipd\Desktop\DataBase2\WM_Library\Uploads\title_data.csv'
with(
	format = 'CSV',
	firstrow = 1,
	rowterminator = '\n'
)
;
go


select *
	from Collection.tblTitle
;
go


/* 6. Import Collection.tblCopy */

ALTER TABLE Collection.tblCopy
	alter column OnLoan nchar(1) null;

BULK INSERT Collection.tblCopy
FROM 'C:\Users\ipd\Desktop\DataBase2\WM_Library\Uploads\copy_data.csv'
with(
	format = 'CSV',
	firstrow = 1,
	rowterminator = '\n'
)
;
go


select *
	from Collection.tblCopy
;
go


/* 7. Import Collection.tblItem */

/* ALTER tblItem */
ALTER TABLE Collection.tblItem
	alter column Loanable nchar(1) null;

BULK INSERT Collection.tblItem
FROM 'C:\Users\ipd\Desktop\DataBase2\WM_Library\Uploads\item_data.csv'
with(
	format = 'CSV',
	firstrow = 1,
	rowterminator = '\n'
)
;
go

select *
	from Collection.tblItem
;
go


/* 8. Import Services.tblReservations */
BULK INSERT Services.tblReservations
FROM 'C:\Users\ipd\Desktop\DataBase2\WM_Library\Uploads\reservation_data.csv'
with(
	format = 'CSV',
	firstrow = 1,
	rowterminator = '\n'
)
;
go

select *
	from Services.tblReservations
;
go


/* 9. Import Services.tblLoan */
BULK INSERT Services.tblLoan
FROM 'C:\Users\ipd\Desktop\DataBase2\WM_Library\Uploads\loan_data.csv'
with(
	format = 'CSV',
	firstrow = 1,
	rowterminator = '\n'
)
;
go

select *
	from Services.tblLoan
;
go


/* 10. Import Services.tblLoanHistory */
BULK INSERT Services.tblLoanHistory
FROM 'C:\Users\ipd\Desktop\DataBase2\WM_Library\Uploads\loanhist_data.csv'
with(
	format = 'CSV',
	firstrow = 1,
	rowterminator = '\n'
)
;
go

select *
	from Services.tblLoanHistory
;
go

