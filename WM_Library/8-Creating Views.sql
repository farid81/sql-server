/* Creating Views for WM_Library 
	script Date: March 7, 2019
	Developed by: Farid Eslambolchi, Dmitryi Kryukov, Varut Wongsuwan, Tyler Beland
*/

use WM_Library
;
go

/*4. Create a view and save it as adultwideView that queries the member and adult tables.
Lists the name & address for all adults.*/

if OBJECT_ID('Membership.adultwideView ', 'V') is not null
	drop view Membership.adultwideView
;
go

-- Creating Membership.adultwideView 
create view Membership.adultwideView 
WITH ENCRYPTION
as
SELECT concat_ws(' ', MM.FirstName, MM.MiddleInitial, MM.LastNAme) as "Full Name",
concat_ws(', ', MA.Address, MA.City, MA.State, MA.ZIP) as "Full Address"
FROM Membership.tblMembers as MM
INNER JOIN Membership.tblAdults MA
ON  MM.MemberID = MA.MemberID
;
go

select *
from Membership.adultwideView 
;
go




/*5. Create a view and save it as ChildwideView that queries the member, adult, and
juvenile tables. Lists the name & address for the juveniles.*/

if OBJECT_ID('Membership.ChildwideView ', 'V') is not null
	drop view Membership.ChildwideView
;
go

create view Membership.ChildwideView 
WITH ENCRYPTION
as
	SELECT	concat_ws(' ', MM.FirstName, MM.MiddleInitial, MM.LastName) as "Full Name",
			concat_ws(', ', MA.Address, MA.City, MA.State, MA.ZIP) as "Full Address"
	FROM Membership.tblJuveniles as MJ
		INNER JOIN Membership.tblMembers as MM
			ON MJ.MemberID = MM.MemberID
		LEFT OUTER JOIN Membership.tblAdults MA
			ON  MJ.AdultMemberID = MA.MemberID
	GROUP BY MM.MemberID, concat_ws(' ', MM.FirstName, MM.MiddleInitial, MM.LastName),
	concat_ws(', ', MA.Address, MA.City, MA.State, MA.ZIP)
;
go

select *
from Membership.ChildwideView 
;
go


/*6. Create a view and save it as and CopywideView that queries the copy, title and item
tables. Lists complete information about each copy.*/

if OBJECT_ID('Collection.CopywideView ', 'V') is not null
	drop view Collection.CopywideView
;
go

CREATE view Collection.CopywideView 
WITH ENCRYPTION
as
	SELECT CC.ISBN, CT.Title, CI.Translation, CI.Cover, CI.Loanable, CC.OnLoan, CT.Synopsis
	FROM Collection.tblCopy as CC
	INNER JOIN Collection.tblItem AS CI
	ON CC.ISBN = CI.ISBN
	INNER JOIN Collection.tblTitle as CT
	ON CI.TitleID = CT.TitleID
;
go

select *
from Collection.CopywideView 
;
go


/*7. Create a view and save it as LoanableView that queries CopywideView (3-table join).
Lists complete information about each copy marked as loanable (loanable = 'Y').*/

if OBJECT_ID('Services.LoanableView ', 'V') is not null
	drop view Services.LoanableView
;
go


CREATE VIEW Services.LoanableView
WITH ENCRYPTION
as
	SELECT * 
	FROM Collection.CopywideView WHERE Loanable='Y';
;
go

select *
from Services.LoanableView
;
go



/* 8.Create a view and save it as OnshelfView that queries CopywideView (3-table join).
Lists complete information about each copy that is not currently on loan (on_loan ='N').*/

if OBJECT_ID('Services.OnshelfView ', 'V') is not null
	drop view Services.OnshelfView
;
go

CREATE VIEW Services.OnshelfView
WITH ENCRYPTION
as
	SELECT * 
	FROM Collection.CopywideView
	WHERE OnLoan='N';
;
go

select *
from Services.OnshelfView
;
go


/*9. Create a view and save it as OnloanView that queries the loan, title, and member
tables. Lists the member, title, and loan information of a copy that is currently on loan.*/

if OBJECT_ID('Services.OnloanView ', 'V') is not null
	drop view Services.OnloanView
;
go


CREATE VIEW Services.OnloanView
WITH ENCRYPTION
as
	SELECT 
		SL.ISBN,
		CT.Title,
		CI.Translation,
		CONCAT_WS(' ', MM.FirstName, MM.MiddleInitial, MM.LastName) as "On Loan By",
		convert(nvarchar(12),SL.DueDate) as 'Due Date'
	FROM Services.tblLoan as SL
	INNER JOIN Collection.tblCopy as CC
	ON SL.ISBN = CC.ISBN
	INNER JOIN Collection.tblItem CI
	ON CC.ISBN = CI.ISBN
	INNER JOIN Collection.tblTitle CT
	ON CI.TitleID = CT.TitleID
	INNER JOIN Membership.tblMembers MM
	ON SL.MemberID = MM.MemberID
	WHERE CC.OnLoan='Y'
	GROUP BY SL.ISBN,CT.Title, CONCAT_WS(' ', MM.FirstName, MM.MiddleInitial, MM.LastName),
		CI.Translation, SL.DueDate
;
go


select *
from Services.OnloanView
;
go


/*10. Create a view and save it as OverdueView that queries OnloanView (3-table join.)
Lists the member, title, and loan information of a copy on loan that is overdue (due_date
< current date).*/

if OBJECT_ID('Services.OverdueView ', 'V') is not null
	drop view Services.OverdueView
;
go


CREATE VIEW Services.OverdueView
WITH ENCRYPTION
as
select *
from Services.OnloanView
where [Due Date] < getdate()
;
go


select *
from Services.OverdueView
;
go


