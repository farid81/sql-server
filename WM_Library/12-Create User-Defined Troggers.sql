/* Create User-Definied Trigger for WM_Library 
	script Date: March 8, 2019
	Developed by: Farid Eslambolchi, Dmitryi Kryukov, Varut Wongsuwan, Tyler Beland
*/

use WM_Library
;
go

--creating notification that Juvenile Member has turned to 18

create trigger Membership.JuvenileTurnsToAdultTr
on Membership.tblJuveniles
for insert, update, delete
as
	begin
		--declare variables
		declare @BirthDate as datetime
		--compute return value
		select @BirthDate = BirthDate 
		from inserted	

		-- making decision
		if (abs(datediff(year, @BirthDate, getdate() )) >= 18) 
			begin

				raiserror('The Juvenile Member has Turned to 18 years Old', 10, 1)
						
			end
	end
;
go


drop trigger Membership.JuvenileTurnsToAdultTr
