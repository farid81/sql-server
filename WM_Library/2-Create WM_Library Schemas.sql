/* Create Schemas WM_Library 
	script Date: February 27, 2019
	Developed by: Farid Eslambolchi, Dmitryi Kryukov, Varut Wongsuwan, Tyler Beland
*/

use WM_Library
;
go

--creating schemas(Membership, Services, Collections)

create schema Membership authorization dbo
;
go

create schema Services authorization dbo
;
go

create schema Collection authorization dbo
;
go

